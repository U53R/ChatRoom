import java.awt.BorderLayout;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Server extends JFrame{
public static JTextField userText;
public static JTextArea chatWindow;
private ServerSocket server;
private Socket connection;
private static ArrayList<ObjectOutputStream> arr = new ArrayList<ObjectOutputStream>();
private int a = 0;

public Server() {
	super("Messenger");
	userText = new JTextField();
	userText.setEditable(false);
	//userText.addActionListener(
			//new ActionListener() {
		//public void actionPerformed(ActionEvent event) {
			//sendMessage(event.getActionCommand());
			//userText.setText("");
		//}

	//});
	add(userText, BorderLayout.NORTH);
	chatWindow = new JTextArea();
	add(new JScrollPane(chatWindow));
	setSize(300,150);
	setVisible(true);
	
	
}

public void startRunning() {
	try {
		server = new ServerSocket(6781, 100);
	} catch (IOException e) {
		e.printStackTrace();
	}
	while(true) {
		connect();
	}
}

private void connect() {
	try {
		connection = server.accept();
		arr.add(new ObjectOutputStream(connection.getOutputStream()));
		
	} catch (IOException e) {
		e.printStackTrace();
	}
	new Thread(new socket(connection, a)).start();
	a++;
}

public static void sendToAll(String message, int number) {
	for (int i = 0; i < arr.size(); i++) {
		if (i != number) {
		try {
			arr.get(i).writeObject(message);
			arr.get(i).flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	}
}



}
